#include <cmath>
#include <iostream>
using namespace std;

struct DualValue {
  DualValue(float v_=0, float d_=0):
    value(v_),
    derivative(d_){}
  
  float value;
  float derivative;
};

DualValue sin(const DualValue& src) {
  return DualValue(sinf(src.value), cosf(src.value)*src.derivative);
}

DualValue cos(const DualValue& src) {
  return DualValue(cosf(src.value), -sinf(src.value)*src.derivative);
}

DualValue exp(const DualValue& src) {
  return DualValue(expf(src.value), expf(src.value)*src.derivative);
}

DualValue log(const DualValue& src) {
  return DualValue(logf(src.value), 1./(src.value)*src.derivative);
}

DualValue operator *(const DualValue& a, const DualValue& b){
  return DualValue(a.value*b.value, a.value*b.derivative + b.value*a.derivative);
}

DualValue operator /(const DualValue& a, const DualValue& b){
  return DualValue(a.value/b.value, (a.derivative*b.value - b.derivative*a.value)/pow(b.value,2));
}

DualValue operator +(const DualValue& a, const DualValue& b){
  return DualValue(a.value+b.value, a.derivative+b.derivative);
}

DualValue operator -(const DualValue& a, const DualValue& b){
  return DualValue(a.value-b.value, a.derivative-b.derivative);
}

DualValue myFunction(DualValue val) {
  return exp(sin(val)/cos(val) + cos(val)*sin(val));
}

int main () {
  DualValue evaluation_point(0.5,1);
  DualValue result = myFunction(evaluation_point);
  cerr << "value of f: " << result.value << " derivative: " << result.derivative << endl;

  DualValue epsilon(1e-3,0);
  DualValue before = evaluation_point+epsilon;
  DualValue after  = evaluation_point-epsilon;
  DualValue numdiff = (myFunction(before)-myFunction(after))/(DualValue(2.)*epsilon);
  cerr << " numdiff: " << numdiff.value << endl;
  
}


    
